@ProtocolEditor
Feature: Protocol Editor

    Subcase_Protocol_Editor
    Total_TestCase_9
    TestCase_103_105_104_229_230_231_232_233_310

    Subcase_Run_Protocol
    Total_TestCase_9
    TestCase_25_28_32_30_225_226_227_228_312

    Subcase_Tube
    Total_TestCase_8
    TestCase_6_7_11_12_309_55_57_58

    Subcase_Chip
    Total_TestCase_2
    TestCase_9_59

    Subcase_Rack_Definition
    Total_TestCase_9
    Test_Case_87_5_89_90_92_95_311_99_100

    @MSP-103 @SubcaseProtocolEditor @Positive @Medium
    Scenario: MSP-103:Open Profile
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-103" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF MAKE NEW PROFILE
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc - Tester" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        When user doubleclick "Tester MSP-103" button
        Then validate user current position is on protocol editor page
        When user click on "Queue" tab
        Then validate "View Details" button is "enabled"
        And validate "New Profile" button is "enabled"
        When user click "View Details" button
        Then validate user current position is on new profile page
        And validate "Close" button is "enabled"
        And validate "Edit" button is "enabled"
        When user click "Close" button
        Then validate user current position is on protocol editor page
        ## END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-103" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-105 @SubcaseProtocolEditor @Positive @Medium
    Scenario: MSP-105:Remove Profile
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-105" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF MAKE NEW PROFILE
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc - Tester" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user doubleclick "Tester MSP-105" button
        Then validate user current position is on protocol editor page
        When user click option button on the profile card
        Then validate "Skip from queue" is "appeared"
        And validate "Remove" is "appeared"
        And validate "Skip from queue" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        When user click on "Remove" button
        Then validate popup "Remove Profile" is "appeared"
        When user click "remove" button
        And user click "Save" button
        ## END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-105" on the screen
        And user click three dots button
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-104 @SubcaseProtocolEditor @Positive @Medium
    Scenario: MSP-104:Skip Profile From Queue
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-104" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF MAKE NEW PROFILE
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc - Tester" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user doubleclick "Tester MSP-104" button
        Then validate user current position is on protocol editor page
        When user click option button on the profile card
        Then validate "Skip from queue" is "appeared"
        And validate "Remove" is "appeared"
        And validate "Skip from queue" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        When user click on "Skip from queue" button
        And validate "Two Conc - Tester" profile card background color changes to "rgba(202, 202, 202, 1)"
        And validate "View Details" button is "enabled"
        When user click option button on the profile card
        Then validate "Return to queue" is "appeared"
        And validate "Remove" is "appeared"
        And validate "Return to queue" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        When user click on "Return to queue" button
        And validate "Two Conc - Tester" profile cards background color changes to "rgba(255, 255, 255, 1)"
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-104" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-229 @SubcaseProtocolEditor @Positive @Medium
    Scenario: MSP-229:View Selected Tubes each profile
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-229" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 2
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "2" on tube "A1"
        Then validate popup "Deck 2 (A1)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 30 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF DECK SET UP - 4
        And user click deck "2" on tube "A3"
        Then validate popup "Deck 2 (A3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Buffer" on liquid type
        And user input 25 on starting volume
        And user click "apply" button
        ### END OF DECK SET UP - 4
        ### START OF MAKE NEW PROFILE - 1
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc - Tester" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 1
        ### START OF MAKE NEW PROFILE - 2
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "BE Tube - Tester" as profile name
        And select "300 kDa" as MWCO
        And select "Buffer Exchange" as method
        And select "Tube" as buffer source
        And user click "Left" tube on the required labware
        And user click deck "2" on tube "A1"
        And user click "Right" tube on the required labware
        And user click deck "2" on tube "A3"
        And user input 5 on final volume
        And user input 25 on start exchange
        And user input 5 on step size
        And user input 0.6 on est final concentration
        And user input 25 on exchange volume
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 2
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-229" on the screen
        Then validate "Tester MSP-229" is highlighted with color "rgba(204, 225, 253, 1)"
        And validate "Open" button is "enabled"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user click on "2. BE Tube - Tester" card
        Then validate "2. BE Tube - Tester" is highlighted with colour "rgba(204, 225, 253, 1)"
        When user scroll down page
        When user click the "Deck" on the screen
        Then validate tube "2-A1" color is "rgba(247, 162, 70, 1)"
        And validate tube "2-A3" color is "rgba(51, 117, 53, 1)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-229" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL
        # COMPLETE / LENGKAP


    @MSP-230 @SubcaseProtocolEditor @Positive @Medium
    Scenario: MSP-230:Search Labware with Correct Name
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-230" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        ### END OF MAKE NEW PROTOCOL
        ### START OF TEST CASE STEPS
        And user click the "Tester MSP-230" on the screen
        Then validate "Open" button is "enabled"
        Then validate "Tester MSP-230" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user click on "Labware" tab
        And user input "1.5mL Conical Tube 4x6" on labware search box
        Then validate "1.5mL Conical Tube 4x6" is "appeared"
        When user clear the labware search box
        Then validate "15mL Conical Tube 3x4" is "appeared"
        And validate "1.5mL Conical Tube 4x6" is "appeared"
        And validate "50mL Conical Tube 2x3" is "appeared"
        And validate "Chip Caddy" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-230" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-231 @SubcaseProtocolEditor @Negative @Low
    Scenario: MSP-231:Search Labware with Incorrect Name
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-231" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        ### END OF MAKE NEW PROTOCOL
        ### START OF TEST CASE STEPS
        And user click the "Tester MSP-231" on the screen
        Then validate "Open" button is "enabled"
        Then validate "Tester MSP-231" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user click on "Labware" tab
        And user input "wrong input" on labware search box
        Then validate "15mL Conical Tube 3x4" is "disappeared"
        And validate "1.5mL Conical Tube 4x6" is "disappeared"
        And validate "50mL Conical Tube 2x3" is "disappeared"
        And validate "Chip Caddy" is "disappeared"
        When user clear the labware search box
        Then validate "15mL Conical Tube 3x4" is "appeared"
        And validate "1.5mL Conical Tube 4x6" is "appeared"
        And validate "50mL Conical Tube 2x3" is "appeared"
        And validate "Chip Caddy" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-231" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-232 @SubcaseProtocolEditor @Positive @Medium @Regression
    Scenario: MSP-232:Rename the Protocol and Save the changes
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### START OF TEST CASE STEPS
        When user click the "Add Protocol" on the screen
        Then validate "Add Protocol" is highlighted with color "rgba(204, 225, 253, 1)"
        And validate "Open" button is "enabled"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user doubleclick on protocol title
        And user input "Tester MSP-232" on protocol title
        And user click "Save" button
        Then validate popup "Protocols have been saved." is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-232" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-233 @SubcaseProtocolEditor @Negative @Low
    Scenario: MSP-233:Rename the Protocol and Discard the changes
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-233" on protocol title
        And user click "Save" button
        ### END OF MAKE NEW PROTOCOL
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-233" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-233" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user doubleclick on protocol title
        And user input "Tester MSP-233 Edited" on protocol title
        And user click on "Labware" tab
        Then validate "Tester MSP-233 Edited" is "appeared"
        When user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        And validate "Tester MSP-233" is "appeared"
        When user doubleclick "Tester MSP-233" button
        Then validate user current position is on protocol editor page
        And validate "Tester MSP-233" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-233" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-310 @SubcaseProtocolEditor @Negative @Low
    Scenario: MSP-310:Rename Protocol with Unvalid Value
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-310" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        ### END OF MAKE NEW PROTOCOL
        ### START OF TEST CASE STEPS
        And user click the "Tester MSP-310" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-310" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        And validate "Tester MSP-310" is "appeared"
        When user doubleclick on protocol title
        And user input "Protocol #%*:<>./?!,|" on protocol title
        And user click on "Labware" tab
        Then validate error popup with message: "cannot contain any of following characters" is appeared
        And validate "Tester MSP-310" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-310" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-25 @SubcaseRunProtocol @Positive @Medium
    Scenario: MSP-25:Save the Updated Protocol
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-25" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        ### END OF MAKE NEW PROTOCOL
        ### START OF TEST CASE STEPS
        And user click the "Tester MSP-25" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-25" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user click on "Labware" tab
        And user scroll down page
        And user click the "Deck" on the screen
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 2
        And user click "Save" button
        Then validate popup "Changes have been saved." is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-25" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-28 @SubcaseRunProtocol @Positive @Medium @RUN
    Scenario: MSP-28:Run the Saved Protocol
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-28" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user scroll down page
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "Chip Caddy" and drop to rack number 9
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "9" on tube "A3"
        Then validate popup "Deck 9 (A3)" is "appeared"
        When user select "100 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF DECK SET UP - 4
        And user click deck "9" on tube "A4"
        Then validate popup "Deck 9 (A4)" is "appeared"
        When user select "300 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 4
        ### START OF MAKE NEW PROFILE - 1
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 1
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-28" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-28" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        ### START OF TEST CASE STEPS
        When user click "Run" button
        Then validate "PAUSE ALL" button is "enabled"
        ### START OF DELETE PROTOCOL
        When user click "PAUSE ALL" button
        And user click Stop button to stop the execution of a protocol
        And user click "Stop" button
        And user click "RETURN LABWARE" button
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-28" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-32 @SubcaseRunProtocol @Positive @Medium @RUN
    Scenario: MSP-32:Run without Save Protocol
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-32" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user scroll down page
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "Chip Caddy" and drop to rack number 9
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "9" on tube "A3"
        Then validate popup "Deck 9 (A3)" is "appeared"
        When user select "100 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF DECK SET UP - 4
        And user click deck "9" on tube "A4"
        Then validate popup "Deck 9 (A4)" is "appeared"
        When user select "300 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 4
        ### START OF MAKE NEW PROFILE - 1
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 1
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-32" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-32" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        ### START OF TEST CASE STEPS
        When user click on "Labware" tab
        And wait for loading process
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 12
        Then validate "Save" button is "enabled"
        When user click "Run" button
        Then validate popup "Running Protocol" is "appeared"
        And validate "cancel" button is "enabled"
        And validate "Save & Run" button is "enabled"
        And validate "run" button is "enabled"
        When user click "run" button
        Then validate "PAUSE ALL" button is "enabled"
        ### START OF DELETE PROTOCOL
        When user click "PAUSE ALL" button
        And user click Stop button to stop the execution of a protocol
        And user click "Stop" button
        And user click "RETURN LABWARE" button
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-32" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-30 @SubcaseRunProtocol @Positive @Medium @RUN @BUG
    Scenario: MSP-30:Save and Run the Protocol
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-30" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user scroll down page
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "Chip Caddy" and drop to rack number 9
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "9" on tube "A3"
        Then validate popup "Deck 9 (A3)" is "appeared"
        When user select "100 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF DECK SET UP - 4
        And user click deck "9" on tube "A4"
        Then validate popup "Deck 9 (A4)" is "appeared"
        When user select "300 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 4
        ### START OF MAKE NEW PROFILE - 1
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 1
        ### START OF TEST CASE STEPS
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-30" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-30" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        # And validate "Save" button is "disabled"
        And validate "Run" button is "enabled"
        When user click on "Labware" tab
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 12
        Then validate "Save" button is "enabled"
        When user click "Run" button
        Then validate popup "Running Protocol" is "appeared"
        And validate "The protocol has not been saved, run the protocol?" is "appeared"
        And validate "cancel" button is "enabled"
        And validate "Save & Run" button is "enabled"
        And validate "run" button is "enabled"
        When user click "Save & Run" button
        Then validate "PAUSE ALL" button is "enabled"
        ### START OF DELETE PROTOCOL
        When user click "PAUSE ALL" button
        And user click Stop button to stop the execution of a protocol
        And user click "Stop" button
        And user click "RETURN LABWARE" button
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-30" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-225 @SubcaseRunProtocol @Negative @Medium
    Scenario: MSP-225:Run Protocol without define Pulse Station
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-225" on protocol title
        Then validate user current position is on protocol editor page
        When user scroll down page
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "Chip Caddy" and drop to rack number 9
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "9" on tube "A3"
        Then validate popup "Deck 9 (A3)" is "appeared"
        When user select "100 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF DECK SET UP - 4
        And user click deck "9" on tube "A4"
        Then validate popup "Deck 9 (A4)" is "appeared"
        When user select "300 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 4
        ### START OF MAKE NEW PROFILE - 1
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 1
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-225" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-225" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        And validate "Run" button is "enabled"
        When user click "Run" button
        And validate "Pulse station is empty" is "appeared"
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-225" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL

        
    @MSP-226 @SubcaseRunProtocol @Negative @Medium
    Scenario: MSP-226:Run protocol without define Chip
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-226" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user scroll down page
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "Chip Caddy" and drop to rack number 9
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF MAKE NEW PROFILE - 1
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 1
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-226" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-226" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        And validate "Run" button is "enabled"
        When user click "Run" button
        And validate "Chips are empty" is "appeared"
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-226" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-227 @SubcaseRunProtocol @Negative @Medium
    Scenario: MSP-227:Run protocol without create profile
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-227" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user scroll down page
        And user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "Chip Caddy" and drop to rack number 9
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "9" on tube "A3"
        Then validate popup "Deck 9 (A3)" is "appeared"
        When user select "100 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF DECK SET UP - 4
        And user click deck "9" on tube "A4"
        Then validate popup "Deck 9 (A4)" is "appeared"
        When user select "300 kDa" on chip option
        And user click "apply" button
        And user click "Save" button
        ### END OF DECK SET UP - 4
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-227" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-227" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        And validate "Run" button is "disabled"
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-227" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-228 @SubcaseRunProtocol @Negative @Medium
    Scenario: MSP-228:Run Protocol when all profiles are skipped
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-228" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 2
        And user perform drag "Chip Caddy" and drop to rack number 9        
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "2" on tube "A1"
        Then validate popup "Deck 2 (A1)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 30 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF DECK SET UP - 4
        And user click deck "2" on tube "A3"
        Then validate popup "Deck 2 (A3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Buffer" on liquid type
        And user input 25 on starting volume
        And user click "apply" button
        ### END OF DECK SET UP - 4
        ### START OF DECK SET UP - 5
        And user click deck "9" on tube "A3"
        Then validate popup "Deck 9 (A3)" is "appeared"
        When user select "100 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 5
        ### START OF DECK SET UP - 6
        And user click deck "9" on tube "A4"
        Then validate popup "Deck 9 (A4)" is "appeared"
        When user select "300 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 6
        ### START OF MAKE NEW PROFILE - 1
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc - Tester" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE - 1
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-228" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-228" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user click option button on the profile card
        Then validate "Skip from queue" is "appeared"
        And validate "Remove" is "appeared"
        And validate "Skip from queue" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        When user click on "Skip from queue" button
        Then validate "Two Conc - Tester" profile card background color changes to "rgba(202, 202, 202, 1)"
        And validate "Run" button is "enabled"
        When user click "Run" button
        Then validate popup "Running Protocol" is "appeared"
        When user click "run" button
        Then validate "Failed to run protocol" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-228" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL
        # COMPLETE / LENGKAP


    @MSP-312 @SubcaseRunProtocol @Negative @Medium
    Scenario: MSP-312:Run Protocol After Removing Profile Tube Rack
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-312" on protocol title
        And user click "Pulse Station 1" edit button
        And user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "15 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "15mL"
        And validate "righttubeadapter" on pulse station 1 have volume "15mL"
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 3
        And user perform drag "Chip Caddy" and drop to rack number 9        
        ### END OF MAKE NEW PROTOCOL
        ### START OF DECK SET UP - 1
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 10 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 1
        ### START OF DECK SET UP - 2
        And user click deck "3" on tube "B3"
        Then validate popup "Deck 3 (B3)" is "appeared"
        When user click the switch tube button to activate the input box
        And user select "Sample" on liquid type
        And user input 15 on starting volume
        And user input 0.1 on starting concentration
        And user click "apply" button
        ### END OF DECK SET UP - 2
        ### START OF DECK SET UP - 3
        And user click deck "9" on tube "A3"
        Then validate popup "Deck 9 (A3)" is "appeared"
        When user select "100 kDa" on chip option
        And user click "apply" button
        ### END OF DECK SET UP - 3
        ### START OF MAKE NEW PROFILE
        And user click on "Queue" tab
        And user click "New Profile" button
        Then validate user current position is on new profile page
        When user input "Two Conc - Tester" as profile name
        And select "100 kDa" as MWCO
        And select "Concentrate" as method
        And user click "Left" tube on the required labware
        And user click deck "3" on tube "B3"
        And user click "Right" tube on the required labware
        And user click deck "1" on tube "C2"
        And user input 5 on final volume
        And user input 0.5 on est final concentration
        And user click "Create" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        ### END OF MAKE NEW PROFILE
        ### START OF TEST CASE STEPS
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-312" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-312" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        When user scroll down page
        And user click the "Deck" on the screen
        And user click the thrash button on tube rack number 1
        Then validate "Remove Labware" is "appeared"
        And validate "remove" button is "enabled"
        And validate "cancel" button is "enabled"
        When user click "remove" button
        And user click the thrash button on tube rack number 3
        Then validate "Remove Labware" is "appeared"
        And validate "remove" button is "enabled"
        And validate "cancel" button is "enabled"
        When user click "remove" button
        Then validate user current position is on protocol editor page
        When user click "Save" button
        Then validate popup "Changes have been saved." is "appeared"
        And validate "Run" button is "enabled"
        When user click "Run" button
        Then validate "Tubes are empty" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-312" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-6 @SubcaseTube @Positive @Medium
    Scenario: MSP-6:Define 50mL tube Rack with Sample Liquid type
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-6" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 50 on starting volume
        And user input 0.12 on starting concentration
        Then validate "50" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.12" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A2
        When user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 50 on starting volume
        And user input 0.12 on starting concentration
        Then validate "50" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.12" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A1" has volume "50" mL
        And validate tube "1-A2" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A3
        When user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 50 on starting volume
        And user input 0.12 on starting concentration
        Then validate "50" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.12" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A1" has volume "50" mL
        And validate tube "1-A2" has volume "50" mL
        And validate tube "1-A3" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B1
        When user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 50 on starting volume
        And user input 0.12 on starting concentration
        Then validate "50" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.12" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-A1" has volume "50" mL
        And validate tube "1-A2" has volume "50" mL
        And validate tube "1-A3" has volume "50" mL
        And validate tube "1-B1" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B2
        When user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 50 on starting volume
        And user input 0.12 on starting concentration
        Then validate "50" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.12" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-A1" has volume "50" mL
        And validate tube "1-A2" has volume "50" mL
        And validate tube "1-A3" has volume "50" mL
        And validate tube "1-B1" has volume "50" mL
        And validate tube "1-B2" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B3
        When user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 50 on starting volume
        And user input 0.12 on starting concentration
        Then validate "50" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.12" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-A1" has volume "50" mL
        And validate tube "1-A2" has volume "50" mL
        And validate tube "1-A3" has volume "50" mL
        And validate tube "1-B1" has volume "50" mL
        And validate tube "1-B2" has volume "50" mL
        And validate tube "1-B3" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-6" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-7 @SubcaseTube @Positive @Medium
    Scenario: MSP-7:Define 15mL tubes rack with Sample liquid type
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-7" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A4
        And user click deck "1" on tube "A4"
        Then validate popup "Deck 1 (A4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A4)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B1
        And user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B2
        And user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B3
        And user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-B3" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B4
        And user click deck "1" on tube "B4"
        Then validate popup "Deck 1 (B4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B4)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-B3" has volume "15" mL
        And validate tube "1-B4" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C1
        And user click deck "1" on tube "C1"
        Then validate popup "Deck 1 (C1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C1)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-B3" has volume "15" mL
        And validate tube "1-B4" has volume "15" mL
        And validate tube "1-C1" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C2
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C2)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-B3" has volume "15" mL
        And validate tube "1-B4" has volume "15" mL
        And validate tube "1-C1" has volume "15" mL
        And validate tube "1-C2" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C3
        And user click deck "1" on tube "C3"
        Then validate popup "Deck 1 (C3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C3)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-B3" has volume "15" mL
        And validate tube "1-B4" has volume "15" mL
        And validate tube "1-C1" has volume "15" mL
        And validate tube "1-C2" has volume "15" mL
        And validate tube "1-C3" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C4
        And user click deck "1" on tube "C4"
        Then validate popup "Deck 1 (C4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 15 on starting volume
        And user input 0.1 on starting concentration
        Then validate "15" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.1" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C4)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-B3" has volume "15" mL
        And validate tube "1-B4" has volume "15" mL
        And validate tube "1-C1" has volume "15" mL
        And validate tube "1-C2" has volume "15" mL
        And validate tube "1-C3" has volume "15" mL
        And validate tube "1-C4" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-B4" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C1" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C2" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C3" colour is "rgba(251, 208, 162, 1)"
        And validate tube "1-C4" colour is "rgba(251, 208, 162, 1)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-7" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-11 @SubcaseTube @Positive @Medium
    Scenario: MSP-11:Define 50mL tube Rack with Buffer Liquid type
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-11" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 50 on starting volume
        And user select "Buffer" on liquid type
        Then validate "50" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 50 on starting volume
        And user select "Buffer" on liquid type
        Then validate "50" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A2" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 50 on starting volume
        And user select "Buffer" on liquid type
        Then validate "50" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A3" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B1
        And user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 50 on starting volume
        And user select "Buffer" on liquid type
        Then validate "50" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-B1" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B2
        And user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 50 on starting volume
        And user select "Buffer" on liquid type
        Then validate "50" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-B2" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B3
        And user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 50 on starting volume
        And user select "Buffer" on liquid type
        Then validate "50" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-B3" has volume "50" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(195, 228, 197, 1)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-11" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-12 @SubcaseTube @Positive @Medium
    Scenario: MSP-12:Define 15mL tubes rack with Buffer liquid type
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-12" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A2" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A3" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A4
        And user click deck "1" on tube "A4"
        Then validate popup "Deck 1 (A4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A4)" is "disappeared"
        And validate tube "1-A4" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B1
        And user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-B1" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B2
        And user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-B2" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B3
        And user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-B3" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B4
        And user click deck "1" on tube "B4"
        Then validate popup "Deck 1 (B4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B4)" is "disappeared"
        And validate tube "1-B4" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C1
        And user click deck "1" on tube "C1"
        Then validate popup "Deck 1 (C1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C1)" is "disappeared"
        And validate tube "1-C1" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C2
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C2)" is "disappeared"
        And validate tube "1-C2" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C3
        And user click deck "1" on tube "C3"
        Then validate popup "Deck 1 (C3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C3)" is "disappeared"
        And validate tube "1-C3" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C4
        And user click deck "1" on tube "C4"
        Then validate popup "Deck 1 (C4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Buffer" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "disabled"
        When user input 15 on starting volume
        And user select "Buffer" on liquid type
        Then validate "15" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C4)" is "disappeared"
        And validate tube "1-C4" has volume "15" mL
        And validate tube "1-A1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-A4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-B4" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C1" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C2" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C3" colour is "rgba(195, 228, 197, 1)"
        And validate tube "1-C4" colour is "rgba(195, 228, 197, 1)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-12" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-309 @SubcaseTube @Positive @Medium
    Scenario: MSP-309:Define 1.5mL tubes rack with Sample Liquid type
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-309" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "1.5mL Conical Tube 4x6" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" has volume "1.5" mL
        And validate tube "1-A1" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A2" has volume "1.5" mL
        And validate tube "1-A2" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A3" has volume "1.5" mL
        And validate tube "1-A3" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-A4
        And user click deck "1" on tube "A4"
        Then validate popup "Deck 1 (A4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A4)" is "disappeared"
        And validate tube "1-A4" has volume "1.5" mL
        And validate tube "1-A4" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-A5
        And user click deck "1" on tube "A5"
        Then validate popup "Deck 1 (A5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A5)" is "disappeared"
        And validate tube "1-A5" has volume "1.5" mL
        And validate tube "1-A5" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-A6
        And user click deck "1" on tube "A6"
        Then validate popup "Deck 1 (A6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A6)" is "disappeared"
        And validate tube "1-A6" has volume "1.5" mL
        And validate tube "1-A6" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-B1
        And user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-B1" has volume "1.5" mL
        And validate tube "1-B1" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-B2
        And user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-B2" has volume "1.5" mL
        And validate tube "1-B2" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-B3
        And user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-B3" has volume "1.5" mL
        And validate tube "1-B3" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-B4
        And user click deck "1" on tube "B4"
        Then validate popup "Deck 1 (B4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B4)" is "disappeared"
        And validate tube "1-B4" has volume "1.5" mL
        And validate tube "1-B4" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-B5
        And user click deck "1" on tube "B5"
        Then validate popup "Deck 1 (B5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B5)" is "disappeared"
        And validate tube "1-B5" has volume "1.5" mL
        And validate tube "1-B5" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-B6
        And user click deck "1" on tube "B6"
        Then validate popup "Deck 1 (B6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B6)" is "disappeared"
        And validate tube "1-B6" has volume "1.5" mL
        And validate tube "1-B6" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-C1
        And user click deck "1" on tube "C1"
        Then validate popup "Deck 1 (C1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C1)" is "disappeared"
        And validate tube "1-C1" has volume "1.5" mL
        And validate tube "1-C1" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-C2
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C2)" is "disappeared"
        And validate tube "1-C2" has volume "1.5" mL
        And validate tube "1-C2" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-C3
        And user click deck "1" on tube "C3"
        Then validate popup "Deck 1 (C3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C3)" is "disappeared"
        And validate tube "1-C3" has volume "1.5" mL
        And validate tube "1-C3" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-C4
        And user click deck "1" on tube "C4"
        Then validate popup "Deck 1 (C4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C4)" is "disappeared"
        And validate tube "1-C4" has volume "1.5" mL
        And validate tube "1-C4" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-C5
        And user click deck "1" on tube "C5"
        Then validate popup "Deck 1 (C5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C5)" is "disappeared"
        And validate tube "1-C5" has volume "1.5" mL
        And validate tube "1-C5" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-C6
        And user click deck "1" on tube "C6"
        Then validate popup "Deck 1 (C6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C6)" is "disappeared"
        And validate tube "1-C6" has volume "1.5" mL
        And validate tube "1-C6" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-D1
        And user click deck "1" on tube "D1"
        Then validate popup "Deck 1 (D1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D1)" is "disappeared"
        And validate tube "1-D1" has volume "1.5" mL
        And validate tube "1-D1" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-D2
        And user click deck "1" on tube "D2"
        Then validate popup "Deck 1 (D2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D2)" is "disappeared"
        And validate tube "1-D2" has volume "1.5" mL
        And validate tube "1-D2" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-D3
        And user click deck "1" on tube "D3"
        Then validate popup "Deck 1 (D3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D3)" is "disappeared"
        And validate tube "1-D3" has volume "1.5" mL
        And validate tube "1-D3" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-D4
        And user click deck "1" on tube "D4"
        Then validate popup "Deck 1 (D4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D4)" is "disappeared"
        And validate tube "1-D4" has volume "1.5" mL
        And validate tube "1-D4" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-D5
        And user click deck "1" on tube "D5"
        Then validate popup "Deck 1 (D5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D5)" is "disappeared"
        And validate tube "1-D5" has volume "1.5" mL
        And validate tube "1-D5" colour is "rgba(251, 208, 162, 1)"
        # TUBE 1-D6
        And user click deck "1" on tube "D6"
        Then validate popup "Deck 1 (D6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        When user select "Sample" on liquid type
        Then validate starting volume box is "enabled"
        And validate starting concentration box is "enabled"
        When user input 1.5 on starting volumes
        And user input 0.3 on starting concentration
        Then validate "1.5" is "appeared" on the screen
        When user select "Sample" on liquid type
        Then validate "0.3" is "appeared" on the screen
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D6)" is "disappeared"
        And validate tube "1-D6" has volume "1.5" mL
        And validate tube "1-D6" colour is "rgba(251, 208, 162, 1)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-309" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL

        
    @MSP-55 @SubcaseTube @Positive @Medium
    Scenario: MSP-55:Define 50mL tube Rack with empty tube
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-55" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B1
        And user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B2
        And user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B3
        And user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-55" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-57 @SubcaseTube @Positive @Medium
    Scenario: MSP-57:Define 15mL tube Rack with empty tube
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-57" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "15mL Conical Tube 3x4" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A4
        And user click deck "1" on tube "A4"
        Then validate popup "Deck 1 (A4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A4)" is "disappeared"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B1
        And user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B2
        And user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B3
        And user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B4
        And user click deck "1" on tube "B4"
        Then validate popup "Deck 1 (B4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B4)" is "disappeared"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
       # TUBE 1-C1
        And user click deck "1" on tube "C1"
        Then validate popup "Deck 1 (C1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C1)" is "disappeared"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C2
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C2)" is "disappeared"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
       # TUBE 1-C3
        And user click deck "1" on tube "C3"
        Then validate popup "Deck 1 (C3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C3)" is "disappeared"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C4
        And user click deck "1" on tube "C4"
        Then validate popup "Deck 1 (C4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C4)" is "disappeared"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-57" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-58 @SubcaseTube @Positive @Medium
    Scenario: MSP-58:Define 1.5mL tube Rack with empty tube
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-58" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "1.5mL Conical Tube 4x6" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # TUBE 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate tube "1-A1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A4
        And user click deck "1" on tube "A4"
        Then validate popup "Deck 1 (A4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A4)" is "disappeared"
        And validate tube "1-A4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A5
        And user click deck "1" on tube "A5"
        Then validate popup "Deck 1 (A5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A5)" is "disappeared"
        And validate tube "1-A5" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-A6
        And user click deck "1" on tube "A6"
        Then validate popup "Deck 1 (A6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A6)" is "disappeared"
        And validate tube "1-A6" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B1
        And user click deck "1" on tube "B1"
        Then validate popup "Deck 1 (B1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B1)" is "disappeared"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B2
        And user click deck "1" on tube "B2"
        Then validate popup "Deck 1 (B2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B2)" is "disappeared"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B3
        And user click deck "1" on tube "B3"
        Then validate popup "Deck 1 (B3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B3)" is "disappeared"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B4
        And user click deck "1" on tube "B4"
        Then validate popup "Deck 1 (B4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B4)" is "disappeared"
        And validate tube "1-B4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B5
        And user click deck "1" on tube "B5"
        Then validate popup "Deck 1 (B5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B5)" is "disappeared"
        And validate tube "1-B5" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-B6
        And user click deck "1" on tube "B6"
        Then validate popup "Deck 1 (B6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (B6)" is "disappeared"
        And validate tube "1-B6" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C1
        And user click deck "1" on tube "C1"
        Then validate popup "Deck 1 (C1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C1)" is "disappeared"
        And validate tube "1-C1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C2
        And user click deck "1" on tube "C2"
        Then validate popup "Deck 1 (C2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C2)" is "disappeared"
        And validate tube "1-C2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C3
        And user click deck "1" on tube "C3"
        Then validate popup "Deck 1 (C3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C3)" is "disappeared"
        And validate tube "1-C3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C4
        And user click deck "1" on tube "C4"
        Then validate popup "Deck 1 (C4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C4)" is "disappeared"
        And validate tube "1-C4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C5
        And user click deck "1" on tube "C5"
        Then validate popup "Deck 1 (C5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C5)" is "disappeared"
        And validate tube "1-C5" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-C6
        And user click deck "1" on tube "C6"
        Then validate popup "Deck 1 (C6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (C6)" is "disappeared"
        And validate tube "1-C6" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-D1
        And user click deck "1" on tube "D1"
        Then validate popup "Deck 1 (D1)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D1)" is "disappeared"
        And validate tube "1-D1" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-D2
        And user click deck "1" on tube "D2"
        Then validate popup "Deck 1 (D2)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D2)" is "disappeared"
        And validate tube "1-D2" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-D3
        And user click deck "1" on tube "D3"
        Then validate popup "Deck 1 (D3)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D3)" is "disappeared"
        And validate tube "1-D3" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-D4
        And user click deck "1" on tube "D4"
        Then validate popup "Deck 1 (D4)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D4)" is "disappeared"
        And validate tube "1-D4" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-D5
        And user click deck "1" on tube "D5"
        Then validate popup "Deck 1 (D5)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D5)" is "disappeared"
        And validate tube "1-D5" colour is "rgba(0, 0, 0, 0)"
        # TUBE 1-D6
        And user click deck "1" on tube "D6"
        Then validate popup "Deck 1 (D6)" is "appeared"
        And validate switch tube color button is "rgba(158, 158, 158, 1)"
        And validate liquid type box is "disabled"
        And validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user click the switch tube button to activate the input box
        Then validate liquid type box is "enabled"
        Then validate starting volume box is "disabled"
        And validate starting concentration box is "disabled"
        When user select "Empty" on liquid type
        And validate switch tube color button is "rgba(0, 106, 245, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (D6)" is "disappeared"
        And validate tube "1-D6" colour is "rgba(0, 0, 0, 0)"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-58" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-9 @SubcaseChip @Positive @Medium
    Scenario: MSP-9:Define Chip Rack with Filled Chip
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-9" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "Chip Caddy" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # CHIP 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "5 kDa" on chip option
        Then validate selected mwco option "5 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate "5 kDa" is "appeared"
        # CHIP 1-A2
        And user click deck "1" on tube "A2"
        Then validate popup "Deck 1 (A2)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "10 kDa" on chip option
        Then validate selected mwco option "10 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A2)" is "disappeared"
        And validate "10 kDa" is "appeared"
        # CHIP 1-A3
        And user click deck "1" on tube "A3"
        Then validate popup "Deck 1 (A3)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "30 kDa" on chip option
        Then validate selected mwco option "30 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A3)" is "disappeared"
        And validate "30 kDa" is "appeared"
        # CHIP 1-A4
        And user click deck "1" on tube "A4"
        Then validate popup "Deck 1 (A4)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "50 kDa" on chip option
        Then validate selected mwco option "50 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A4)" is "disappeared"
        And validate "50 kDa" is "appeared"
        # CHIP 1-A5
        And user click deck "1" on tube "A5"
        Then validate popup "Deck 1 (A5)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "100 kDa" on chip option
        Then validate selected mwco option "100 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A5)" is "disappeared"
        And validate "100 kDa" is "appeared"
        # CHIP 1-A6
        And user click deck "1" on tube "A6"
        Then validate popup "Deck 1 (A6)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "300 kDa" on chip option
        Then validate selected mwco option "300 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A6)" is "disappeared"
        And validate "300 kDa" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-9" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-59 @SubcaseChip @Positive @Medium
    Scenario: MSP-59:Edit Defined Chip
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-59" on protocol title
        And user click "Save" button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        When user click the "Deck" on the screen
        And user perform drag "Chip Caddy" and drop to rack number 1
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        # SELECT CHIP 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "5 kDa" on chip option
        Then validate selected mwco option "5 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate "5 kDa" is "appeared"
        # RE-SELECT CHIP 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "50 kDa" on chip option
        Then validate selected mwco option "50 kDa" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        And validate "50 kDa" is "appeared"
        # RE-SELECT CHIP 1-A1
        And user click deck "1" on tube "A1"
        Then validate popup "Deck 1 (A1)" is "appeared"
        And validate mwco option "No Chip" is "enabled"
        And validate mwco option "5 kDa" is "enabled"
        And validate mwco option "10 kDa" is "enabled"
        And validate mwco option "30 kDa" is "enabled"
        And validate mwco option "50 kDa" is "enabled"
        And validate mwco option "100 kDa" is "enabled"
        And validate mwco option "300 kDa" is "enabled"
        And validate "cancel" button is "enabled"
        And validate "apply" button is "enabled"
        When user select "No Chip" on chip option
        Then validate selected mwco option "No Chip" color is "rgba(25, 118, 210, 1)"
        When user click "apply" button
        And wait for loading process
        Then validate "Deck 1 (A1)" is "disappeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        When user click on "Explorer" side menu button
        And user click the "Tester MSP-59" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-87 @RackDefinition @Positive @Medium
    Scenario: MSP-87:Validate Empty Deck
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### START OF TEST CASE STEPS
        When user click the "Add Protocol" on the screen
        Then validate "Add Protocol" is highlighted with color "rgba(204, 225, 253, 1)"
        And validate "Open" button is "enabled"
        When user click "Open" button
        Then validate user current position is on protocol editor page
        And validate "Pulse Station 1" is "appeared"
        When user doubleclick on protocol title
        And user input "Tester MSP-87" on protocol title
        And user click the "Deck" on the screen
        Then validate "1" is "appeared"
        And validate "2" is "appeared"
        And validate "3" is "appeared"
        And validate "4" is "appeared"
        And validate "5" is "appeared"
        And validate "6" is "appeared"
        And validate "7" is "appeared"
        And validate "8" is "appeared"
        And validate "9" is "appeared"
        And validate "10" is "appeared"
        And validate "11" is "appeared"
        And validate "12" is "appeared"
        When user click "Save" button
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-87" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-5 @RackDefinition @Positive @Medium
    Scenario: MSP-5:Add Labware into Empty Deck
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-5" on protocol title
        ### END OF MAKE NEW PROTOCOL
        ### START OF TEST CASE STEPS
        And user click the "Deck" on the screen
        Then validate "1" is "appeared"
        And validate "2" is "appeared"
        And validate "3" is "appeared"
        And validate "4" is "appeared"
        And validate "5" is "appeared"
        And validate "6" is "appeared"
        And validate "7" is "appeared"
        And validate "8" is "appeared"
        And validate "9" is "appeared"
        And validate "10" is "appeared"
        And validate "11" is "appeared"
        And validate "12" is "appeared"
        When user click on "Labware" tab
        Then validate "15mL Conical Tube 3x4" is "appeared"
        And validate "1.5mL Conical Tube 4x6" is "appeared"
        And validate "50mL Conical Tube 2x3" is "appeared"
        And validate "Chip Caddy" is "appeared"
        When user perform drag "50mL Conical Tube 2x3" and drop to rack number 1
        Then validate tube "1-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "1-B3" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "15mL Conical Tube 3x4" and drop to rack number 2
        Then validate tube "2-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "2-C4" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "1.5mL Conical Tube 4x6" and drop to rack number 3
        Then validate tube "3-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-A5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-A6" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-B5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-B6" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-C4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-C5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-C6" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-D1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-D2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-D3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-D4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-D5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "3-D6" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "Chip Caddy" and drop to rack number 4
        Then validate tube "4-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "4-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "4-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "4-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "4-A5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "4-A6" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "15mL Conical Tube 3x4" and drop to rack number 5
        Then validate tube "5-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "5-C4" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "50mL Conical Tube 2x3" and drop to rack number 6
        Then validate tube "6-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "6-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "6-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "6-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "6-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "6-B3" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "Chip Caddy" and drop to rack number 7
        Then validate tube "7-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "7-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "7-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "7-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "7-A5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "7-A6" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "50mL Conical Tube 2x3" and drop to rack number 8
        Then validate tube "8-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "8-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "8-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "8-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "8-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "8-B3" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "Chip Caddy" and drop to rack number 9
        Then validate tube "9-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "9-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "9-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "9-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "9-A5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "9-A6" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "50mL Conical Tube 2x3" and drop to rack number 10
        Then validate tube "10-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "10-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "10-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "10-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "10-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "10-B3" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "1.5mL Conical Tube 4x6" and drop to rack number 11
        Then validate tube "11-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-A4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-A5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-A6" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-B3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-B4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-B5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-B6" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-C1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-C2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-C3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-C4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-C5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-C6" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-D1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-D2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-D3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-D4" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-D5" colour is "rgba(0, 0, 0, 0)"
        And validate tube "11-D6" colour is "rgba(0, 0, 0, 0)"
        When user perform drag "50mL Conical Tube 2x3" and drop to rack number 12
        Then validate tube "12-A1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "12-A2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "12-A3" colour is "rgba(0, 0, 0, 0)"
        And validate tube "12-B1" colour is "rgba(0, 0, 0, 0)"
        And validate tube "12-B2" colour is "rgba(0, 0, 0, 0)"
        And validate tube "12-B3" colour is "rgba(0, 0, 0, 0)"
        ### END OF TEST CASE STEPS
        When user click "Save" button
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-5" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-89 @RackDefinition @Positive @Medium @Test
    Scenario: MSP-89:Validate Hover Labware
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-89" on protocol title
        ### END OF MAKE NEW PROTOCOL
        ### START OF TEST CASE STEPS
        And user click the "Deck" on the screen
        When user click on "Labware" tab
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 1
        And user click the "Station" on the screen
        And wait for loading process
        And user click the "Deck" on the screen
        And user hover the mouse to deck "1" on tube "A1"
        Then validate the "rack-protocoleditor-1" "1" is zoomed in
        When user click on "Labware" tab
        And user perform drag "1.5mL Conical Tube 4x6" and drop to rack number 2
        And user click the "Station" on the screen
        And wait for loading process
        And user click the "Deck" on the screen
        And user hover the mouse to deck "2" on tube "A1"
        Then validate the "rack-protocoleditor-2" "2" is zoomed in
        When user click on "Labware" tab
        And user perform drag "Chip Caddy" and drop to rack number 3
        And user click the "Station" on the screen
        And wait for loading process
        And user click the "Deck" on the screen
        And user hover the mouse to deck "3" on tube "A1"
        Then validate the "rack-protocoleditor-3" "3" is zoomed in
        ### END OF TEST CASE STEPS
        When user click "Save" button
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        And user click the "Tester MSP-89" on the screen
        And user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL

