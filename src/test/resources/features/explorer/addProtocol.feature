@Explorer
Feature: Explorer

    Subcase_Add_Protocol
    Total_TestCase_6
    TestCase_211_8_16_213_313_314

    @MSP-211 @SubcaseAddProtocol @Positive @Medium
    Scenario: MSP-211:Validate Explorer Page
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        And validate three dots button is enabled
        And validate explorer search bar is displayed and enabled
        And validate sort button is clickable
        And validate "Add Protocol" buttons is "enabled"
        And validate "Open" button is "disabled"
        When user doubleclick "Add Protocol" button
        Then validate user current position is on protocol editor page
        And validate "Save" button is "enabled"
        And validate "Run" button is "disabled"
        And validate "Pulse Station 1" is "appeared"
        When user click on "Explorer" side menu button
        Then validate user current position is on explorer page


    @MSP-8 @SubcaseAddProtocol @Positive @Medium @SmokeTest
    Scenario: MSP-8:Create New Protocol from Explorer
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        And validate "Add Protocol" buttons is "enabled"
        And validate "Open" button is "disabled"
        When user doubleclick "Add Protocol" button
        Then validate user current position is on protocol editor page
        And validate "Pulse Station 1" is "appeared"
        When user scroll down the page to find "Cleaning Station" text
        Then validate "Cleaning Station" is "appeared"
        When user click "Save" button
        Then validate popup "Protocols have been saved." is "appeared"
        When user click on "Explorer" side menu button
        Then validate "New Protocol 1" is "appeared"
        ### START OF DELETE PROTOCOL
        When user click the "New Protocol 1" on the screen
        And user click three dots button
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-16 @SubcaseAddProtocol @Positive @Medium
    Scenario: MSP-16:Create New Protocol from Protocol Editor
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Protocol Editor" side menu button
        Then validate user current position is on protocol editor page
        And validate "Labware" is "appeared"
        And validate "Station" is "appeared"
        When user doubleclick on protocol title
        And user input "Tester MSP-16" on protocol title
        And user click "Pulse Station 1" edit button
        Then validate "Pulse Station 1" pop up box is "appeared"
        When user click on "Left Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "50 mL"
        And user click on "Right Tube Adapter" dropdown on pulse station pop up box
        And user clicks on "50 mL"
        And user click "apply" button
        Then validate user current position is on protocol editor page
        And validate "lefttubeadapter" on pulse station 1 have volume "50mL"
        And validate "righttubeadapter" on pulse station 1 have volume "50mL"
        When user click the "Deck" on the screen
        And user perform drag "50mL Conical Tube 2x3" and drop to rack number 1
        And user perform drag "Chip Caddy" and drop to rack number 2
        And user click "Save" button
        Then validate popup "Protocols have been saved." is "appeared"
        When user click on "Explorer" side menu button
        Then validate "Tester MSP-16" is "appeared"
        ### START OF DELETE PROTOCOL
        When user click the "Tester MSP-16" on the screen
        And user click three dots button
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-213 @SubcaseAddProtocol @Positive @Medium
    Scenario: MSP-213:Rename Protocol
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-213" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        When user click the "Tester MSP-213" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-213" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        When user click on "Rename" button
        Then validate popup "Rename Protocol" is "appeared"
        And validate "Tester MSP-213" is "appeared" on the screen
        And validate "Rename" button is "disabled"
        And validate "cancel" button is "enabled"
        When user input "Tester MSP-213 Edited" on the rename protocol box
        Then validate "Tester MSP-213 Edited" is "appeared" on the screen
        And validate "Rename" button is "enabled"
        When user click "Rename" button
        Then validate pop up "Protocol has been renamed" is "appeared"
        And validate "Tester MSP-213 Edited" is "appeared"
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-213 Edited" on the screen
        And user click three dots button
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-313 @SubcaseAddProtocol @Negative @Medium
    Scenario: MSP-313:Rename Protocol with special characters
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-313" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### END OF MAKE NEW PROTOCOL 
        ### START OF TEST CASE STEPS
        When user click the "Tester MSP-313" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-313" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        When user click on "Rename" button
        Then validate popup "Rename Protocol" is "appeared"
        And validate "Tester MSP-313" is "appeared" on the screen
        And validate "Rename" button is "disabled"
        And validate "cancel" button is "enabled"
        When user input "Protocol #%*:<>./?!,|" on the rename protocol box
        Then validate "Protocol #%*:<>./?!,|" is "appeared" on the screen
        And validate error message "cannot contain any of following characters" is "appeared"
        And validate "cannot contain any of following characters" color is "rgba(219, 0, 0, 1)"
        And validate "Rename" button is "disabled"
        When user clear the protocol name box
        And user input "Tester MSP-313 Edited" on the rename protocol box        
        Then validate "Tester MSP-313 Edited" is "appeared" on the screen
        And validate error message "cannot contain any of following characters" is "disappear"
        And validate "Rename" button is "enabled"
        When user click "Rename" button
        Then validate popup "Protocol has been renamed" is "appeared"
        And validate "Tester MSP-313 Edited" is "appeared" on the screen
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-313 Edited" on the screen
        And user click three dots button
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL


    @MSP-314 @SubcaseAddProtocol @Negative @Medium
    Scenario: MSP-314:Rename Protocol with Existing Protocol name
        Given User already on AutoPulse login webpage
        When user click on login button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### MAKE NEW PROTOCOL - 1
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-314 A" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### END OF MAKE NEW PROTOCOL - 1
        ### MAKE NEW PROTOCOL - 2
        When user doubleclick "Add Protocol" button
        And user doubleclick on protocol title
        And user input "Tester MSP-314 B" on protocol title
        And user click "Save" button
        And user click on "Explorer" side menu button
        Then validate user current position is on explorer page
        ### END OF MAKE NEW PROTOCOL - 2
        ### START OF TEST CASE STEPS
        When user click the "Tester MSP-314 B" on the screen
        Then validate "Open" button is "enabled"
        And validate "Tester MSP-314 B" is highlighted with color "rgba(204, 225, 253, 1)"
        When user click three dots button
        Then validate three dots dropdown option is displayed
        And validate "Rename" buttons is "enabled"
        And validate "Remove" buttons is "enabled"
        When user click on "Rename" button
        Then validate popup "Rename Protocol" is "appeared"
        And validate "Tester MSP-314 B" is "appeared" on the screen
        And validate "Rename" button is "disabled"
        And validate "cancel" button is "enabled"
        When user input "Tester MSP-314 A" on the rename protocol box
        Then validate "Tester MSP-314 A" is "appeared" on the screen
        And validate error message "Protocol name already exists" is "appeared"
        And validate "Protocol name already exists" color is "rgba(219, 0, 0, 1)"
        And validate "Rename" button is "disabled"
        When user clear the protocol name box
        And user input "Tester MSP-314 B Edited" on the rename protocol box
        Then validate "Tester MSP-314 B Edited" is "appeared" on the screen
        And validate error message "Protocol name already exists" is "disappear"
        And validate "Rename" button is "enabled"
        When user click "Rename" button
        Then validate popup "Protocol has been renamed" is "appeared"
        And validate "Tester MSP-314 B Edited" is "appeared" on the screen
        ### END OF TEST CASE STEPS
        ### START OF DELETE PROTOCOL - 1
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-314 A" on the screen
        And user click three dots button
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL - 1
        ### START OF DELETE PROTOCOL - 2
        And user click on "Explorer" side menu button
        When user click the "Tester MSP-314 B Edited" on the screen
        And user click three dots button
        And user click on "Remove" button
        And user click "remove" button
        ### END OF DELETE PROTOCOL - 2