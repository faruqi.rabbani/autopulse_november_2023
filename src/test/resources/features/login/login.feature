@Login @RUN
Feature: Login feature

    Check login functionality
    Total_TestCase_6
    TestCase_732_734_736_739_740_746

    Background: The user is already navigated to the login page
        Given the user is on the Login page
        Then the page's title should be "FLO i8® - Login"

    @TC-1 @Positive
    Scenario: TC-1:Login will success with valid credentials
        Precondition : Already created the "administrator" account

        When the user enters the username as "administrator"
        And the user enters the password as "Administrator1"
        And the user click the Login button on the Login Page
        Then the user is navigated to the "home" page
        Then the page's title should be "FLO i8® - Protocol Explorer"

    @Negative
    Scenario: TC-2:Login will not success with invalid credentials
        User are unable to login if credentials are invalid

        When the user enters the username as "admin"
        And the user enters the password as "adminadmin"
        And the user click the Login button on the Login Page
        And wait for 1000 ms
        Then a text as "Invalid Username and Password" is shown on the screen
        Then the page's title should be "FLO i8® - Login"

    @Negative
    Scenario: TC-3:Login will not success with invalid credentials
        User are unable to login if credentials are incomplete

        When the user enters the username as "admin"
        And the user click on the "SIGN IN" button
        Then a text as "Invalid Username and Password" is shown on the screen
        Then the page's title should be "FLO i8® - Login"

    @Negative
    Scenario: TC-4:Login will not success with invalid credentials
        User are unable to login if credentials are incomplete

        When the user enters the password as "admin"
        And the user click on the "SIGN IN" button
        Then a text as "Invalid Username and Password" is shown on the screen
        Then the page's title should be "FLO i8® - Login"

    @Example
    Scenario: The user already logged in
        Precondition: The user is already on the home page

        Given the user already logged in to application with this credentials
            | Username      | Password       |
            | administrator | Administrator1 |
        And wait for 1000 ms
        Then the user is navigated to the "home" page

    @Example
    Scenario: The user already logged in
        - Example of using Datatables that provide custom data to be used
        - Example of using soft-assertion to prevent the automation from stopping whenever an assertion is failed

        Precondition: The user is already on the home page

        Given the user already logged in to application with this credentials
            | Username | Password       |
            | notAdmin | Administrator1 |

        ## For example, a bugged/typo text that will make the script stop.
        ## Therefore we can implement Soft-assertion to prevent it from stopping.
        And optionally validate a text as "Incorrect Username and Password is detected!!" is shown on the screen
        # And optionally validate a text as "Invalid Username and Password" is shown on the screen

        And wait for 1000 ms
        Then a text as "Invalid Username and Password" is shown on the screen
        When the user enters the username as "administrator"
        And the user enters the password as "Administrator1"
        And the user click the Login button on the Login Page
        Then the user is navigated to the "home" page
        ## Don't forget to include this step below in order to re-verify it
        Then re-validate soft-assert

## Todo - Add Swipe?