package infrastructure.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * A class to access global configuration defined in config.properties file
 */
public class Configuration {

    private final String PROPERTIES_FILE = "./src/test/java/infrastructure/config/config.properties";
    private Properties prop;
    public String URL;
    public String browser;
    public Boolean isHeadless;
    public Boolean enableScreenshot;

    public Configuration(){
        initProp();
        loadData();
    }

    public void initProp() {
        this.prop = new Properties();
        try {
            FileInputStream fileStream = new FileInputStream(PROPERTIES_FILE);
            prop.load(fileStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadData(){
        URL = getData("URL");
        browser = getData("browser");
        isHeadless = getBooleanData("isHeadless");
        enableScreenshot = getBooleanData("enableScreenshot");
    }

    private String getData(String key){
        return prop.getProperty(key);
    }

    private Boolean getBooleanData(String key){
        return Boolean.parseBoolean(prop.getProperty(key));
    }

}