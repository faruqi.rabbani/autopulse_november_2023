package stepDefinition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import factory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObject.generalFunction.GeneralAction;
import pageObject.generalFunction.GeneralFetch;
import pageObject.generalFunction.SupportFunction;

public class GeneralStep {

    WebDriver driver;
    Actions action;
    GeneralAction genAction;
    GeneralFetch genFetch;
    SupportFunction sprt;

    public static SoftAssertions softAssert = new SoftAssertions();

    /**
     * Initialize GeneralStep instance using default driver from
     * {@code DriverFactory}.
     */
    public GeneralStep() {
        this.driver = DriverFactory.getDriver();
        this.action = new Actions(this.driver);
        this.genAction = new GeneralAction(driver);
        this.genFetch = new GeneralFetch(driver);
        this.sprt = new SupportFunction(driver);
    }

    /**
     * Initialize GeneralStep instance using custom driver.
     * 
     * @param driver
     */
    public GeneralStep(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(driver);
        this.genAction = new GeneralAction(driver);
        this.genFetch = new GeneralFetch(driver);
        this.sprt = new SupportFunction(driver);
    }

    @When("the user refresh the browser page")
    public void user_refresh_the_browser_page() {
        driver.navigate().refresh();
    }

    @Then("the page's title should be {string}")
    public void validatePageTitle(String expectedTitle) {
        assertEquals(expectedTitle, sprt.getPageTitle());
    }

    @Then("a text as {string} is shown on the screen")
    public void validateTextShown(String exactText) {
        By textLocator = By.xpath("//*[text()='" + exactText + "']");
        assertTrue("A text \"" + exactText + "\" cannot be found", genFetch.isElementExist(textLocator));
    }

    @When("the user click on the {string} button")
    public void user_clicks_on_button(String button) {
        By btnLocator = By.xpath("//button[text()='" + button + "']");
        genAction.clickElement(btnLocator);
    }

    @When("the user click the {string} text on the screen")
    public void userClicks_anyText(String exactText) {
        By textLocator = By.xpath("//*[text()='" + exactText + "']");
        // Validate that the text is present before continuing
        validateTextShown(exactText);
        genAction.clickElement(textLocator);
    }

    @And("wait for loading process")
    public void waitLoadingProcess() throws InterruptedException {
        /*
         * Just a simple thread sleep.
         * Real usage must use dynamic wait to maintain responsiveness.
         */
        Thread.sleep(200);
    }

    @And("wait for {int} ms")
    public void waitSpecifiedTime(int waitTime) throws InterruptedException {
        Thread.sleep(waitTime);
    }

    @Then("the user is navigated to the {string} page")
    public void the_user_is_navigated_to_the_page(String page) {
        if (page.equalsIgnoreCase("home")) {
            String identifiableText = "Protocol Explorer";
            By homeLocator = By.xpath("//*[text()='" + identifiableText + "']");
            assertTrue("The user is not on the " + page + " page.", genFetch.isElementExist(homeLocator));
        }
    }

    /**
     * Used to check whether a text is shown or not
     * without halting the automation script.
     * <br>
     * </br>
     * You Need to implement "<b>Then re-validate soft-assert</b>"
     * gherkin steps in the end to use it properly,
     * otherwise it might miss any (text) Bug/Typo.
     * 
     * @param targetText
     */
    @Then("optionally validate a text as {string} is shown on the screen")
    public void checktextPresence_Optionally(String targetText) {
        By textLocator = By.xpath("//*[text()='" + targetText + "']");
        sprt.setImplicitWaitMillis(500);
        Boolean isFailed = false;
        Boolean isTextShown = genFetch.isElementExist(textLocator);
        isFailed = isTextShown ? false : true;

        if (isFailed) {
            softAssert.fail("==> The text can't be found when verifying '" + targetText + "'");
            // Set checked to false to ensure it's checked later
            sprt.setSoftAssertCheckedStatus(false);
        }
        // If it's not failed, its and indication that the text is present.
        // Therefore, Soft-Assertion is not required anymore.
        sprt.resetImplicitWait();
    }

    @Then("re-validate soft-assert")
    public void soft_assert_checks() {
        sprt.setSoftAssertCheckedStatus(true);
        softAssert.assertAll();
    }


    // ukifar



    // CLICK
    @When("user click {string} button")
    public void user_click_name_button(String button) {
        By locatorBttn = By.xpath("//div[starts-with(text(), \"" + button + "\")]/parent::button");
        genAction.clickElement(locatorBttn);
    }
    @When("user click the {string} on the screen")
    public void user_click_the_name_on_the_screen(String name) {
        By locatorElement = By.xpath("//div[starts-with(text(), \"" + name + "\")]");
        genAction.clickElement(locatorElement);
    }
    @When("user click on {string} button")
    public void user_click_on_name_button(String name) {
        By locatorBttn = By.xpath("//div[starts-with(text(), \"" + name + "\")]");
        genAction.clickElement(locatorBttn);
    }
    @When("user click on {string} side menu button")
    public void user_click_on_Name_side_menu_button(String name) {
        By sideMenu = By.xpath("//div[text()='" + name + "']/ancestor::div[@role='button']");
        genAction.clickElement(sideMenu);
    }
    @When("user clicks on {string}")
    public void user_clicks_on_choice(String choice) {
        By choiceBttn = By.xpath("//li[text()='" + choice + "']");
        genAction.clickElement(choiceBttn);
    }
    @When("user click on {string} tab")
    public void user_click_on_tab_name(String tab) {
         By tabBttn = By.xpath("//*[starts-with(text(), \"" + tab + "\")]");
         genAction.clickElement(tabBttn);
    }



    // DOUBLE CLICK
    @When("user doubleclick {string} button")
    public void user_doubleclick_name_button(String name) {
        By locatorBttn = By.xpath("//div[starts-with(text(), \"" + name + "\")]");
        genAction.doubleClickElement(locatorBttn);
    }
    @When("user doubleclicks {string} button")
    public void user_doubleclicks_name_button(String name) {
        By locatorBttn = By.xpath("//*[starts-with(text(), \"" + name + "\")]/parent::div");
        genAction.doubleClickElement(locatorBttn);
    }



    // TEXT OR ELEMENT APPEAR/DISAPPEAR
    @Then("validate {string} is {string}")
    public void validate_text_is_appearedOrDisappeared(String text, String choice) {
        By locatorText = By.xpath("//*[text()=\"" + text + "\"]");
        switch (choice) {
            case "appeared":
                assertTrue("text is not displayed", genFetch.isElementDisplayeds(locatorText));
                break;
            case "disappeared":
                assertFalse("text is displayed", genFetch.isElementNotExistNoWait(locatorText));
                break;
        }
    }
    @Then("validate {string} is {string} on the screen")
    public void validate_name_is_appearOrDisappear_on_the_screen(String text, String choice) {
        By locatorText = By.xpath("//*[@value=\"" + text + "\"]");
        switch (choice) {
            case "appeared":
                assertTrue("text is not displayed", genFetch.isElementDisplayed(locatorText));
                break;
            case "disappeared":
                assertFalse("text is displayed", genFetch.isElementDisplayed(locatorText));
                break;
        }
    }



    // ERROR MESSAGE APPEAR/DISAPPEAR
    @Then("validate error message {string} is {string}")
    public void validate_error_message_text_is_appearedOrDisappeared(String text, String choice) {
        By locatorText = By.xpath("//*[contains(text(), \"" + text + "\")]");
        switch (choice) {
            case "appeared":
                assertTrue("text is not displayed", genFetch.isElementDisplayed(locatorText));
                break;
            case "disappeared":
                assertFalse("pop up isn't disappeared", genFetch.isElementNotExist(locatorText));
                break;
        }
    }


    
    // BUTTON ENABLE OR DISABLE
    @Then("validate {string} button is {string}")
    public void validate_option_button_is_enableOrdisabled(String option, String choice) {
        By locatorBttn = By.xpath("//div[starts-with(text(), \"" + option + "\")]/parent::button");
        switch (choice) {
            case "disabled":
                assertFalse("button is enabled", genFetch.isElementEnabled(locatorBttn));
                break;
            case "enabled":
                assertTrue("button is disabled", genFetch.isElementEnabled(locatorBttn));
                break;
        }
    }
    @Then("validate {string} buttons is {string}")
    public void validate_option_buttons_is_enableOrdisabled(String option, String choice) {
        By locatorBttn = By.xpath("//div[starts-with(text(), \"" + option + "\")]");
        switch (choice) {
            case "disabled":
                assertFalse("button is enabled", genFetch.isElementEnabled(locatorBttn));
                break;
            case "enabled":
                assertTrue("button is disabled", genFetch.isElementEnabled(locatorBttn));
                break;
        }
    }



    // SCROLL DOWN PAGE
    @When("user scroll down the page to find {string} text")
    public void user_scroll_down_the_page_to_find_searched_text(String text) {
        By locatorText = By.xpath("//*[text()='" + text + "']");
        WebElement element = driver.findElement(locatorText);
        genAction.scrollDownPage(element);
    }
    @When("user scroll down page")
    public void user_scroll_down_page() throws InterruptedException {
        genAction.scrollDownToEndPage();
        genAction.scrollDownToEndPage();
        Thread.sleep(500);
    }



    // POPUP APPEAR OR DISAPPEAR
    @Then("validate pop up {string} is {string}")
    public void validate_pop_up_text_is_appearedOrDisappeared(String text, String choice) {
        By locatorText = By.xpath("//*[text()='" + text + "']");
        switch (choice) {
            case "appeared":
                assertTrue("text is not displayed", genFetch.isElementDisplayed(locatorText));
                break;
            case "disappeared":
                assertFalse("text is displayed", genFetch.isElementDisplayed(locatorText));
                break;
        }
    }
    @Then("validate {string} pop up box is {string}")
    public void validate_name_pop_up_box_is_appearedOrDisappeared(String name, String choice) {
        By locatorText = By.xpath("//h2/descendant::div[text()='" + name + "']");
        switch (choice) {
            case "appeared":
                assertTrue("pop up is not displayed", genFetch.isElementDisplayed(locatorText));
                break;
            case "disappeared":
                assertFalse("pop up displayed", genFetch.isElementDisplayed(locatorText));
                break;
        }
    }
    @Then("validate popup {string} is {string}")
    public void validate_popup_text_is_appearedOrDisappeared(String text, String choice) {
        By locatorText = By.xpath("//*[text()='" + text + "']");
        switch (choice) {
            case "appeared":
                assertTrue("text is not displayed", genFetch.isElementDisplayeds(locatorText));
                break;
            case "disappeared":
                assertFalse("text is displayed", genFetch.isElementDisplayeds(locatorText));
                break;
        }
    }



    // DRAG AND DROP
    @When("user perform drag {string} and drop to rack number {int}")
    public void user_perform_drag_name_and_drop_to_rack_number_name(String labware, int rack) {
        By labwareLoc = By.xpath("//div[text()='" + labware + "']/ancestor::div[contains(@id, 'singlelabware')]");
        By rackLoc = By.xpath("//div[contains(@class, 'DeckLayout_deck')][" + rack + "]");

        genAction.dragAndDrop(labwareLoc, rackLoc);
    }



    // HIGHLIGHTED
    @Then("validate {string} is highlighted with color {string}")
    public void element_is_highlighted(String element, String color) {
        By locator = By.xpath("//div[starts-with(text(),\"" + element + "\")]/ancestor::div[@class='protocol selected']");
     
        Assert.assertTrue(genFetch.isElementHighlighted(locator, color));
    }
    @Then("validate {string} is highlighted with colour {string}")
    public void validate_card_is_highlighted_with_colour_choice(String element, String colour) {
        By locator = By.xpath("//div[contains(.,\"" + element + "\")]/ancestor::div[@class='card  card-selected ']");
     
        Assert.assertTrue(genFetch.isElementHighlighted(locator, colour));
    }



    // TEXT COLOR
    @Then("validate {string} color is {string}")
    public void validate_text_color_is_color(String text, String color) {
        By locator = By.xpath("//*[contains(text(), \"" + text + "\")]");

        Assert.assertTrue(genFetch.verifyTextColor(locator, color));
    }



    //HOVER
    @When("user hover the mouse to deck {string} on tube {string}")
    public void user_hover_the_mouse_to_location(String deck, String tube) {
    By locator = By.xpath("//*[@id=\"" + deck + '-' + tube + "\"]");
 
    genAction.moveElement(locator);
    }


}