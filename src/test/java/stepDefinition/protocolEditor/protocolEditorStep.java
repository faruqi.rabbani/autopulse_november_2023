package stepDefinition.protocolEditor;

import org.junit.Assert;

import factory.DriverFactory;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
// import pageObject.explorer.ExplorerPage;
import pageObject.protocolEditor.NewProfilePage;
import pageObject.protocolEditor.ProtocolEditorPage;
import pageObject.runtimeStatus.RuntimeStatusPage;

public class protocolEditorStep {

    // private ExplorerPage explorerPage;
    private ProtocolEditorPage protocolEditorPage;
    private NewProfilePage newProfilePage;
    private RuntimeStatusPage runtimeStatusPage;

    public protocolEditorStep() {
        // explorerPage = new ExplorerPage(DriverFactory.getDriver());
        protocolEditorPage = new ProtocolEditorPage(DriverFactory.getDriver());
        newProfilePage = new NewProfilePage(DriverFactory.getDriver());
        runtimeStatusPage = new RuntimeStatusPage(DriverFactory.getDriver());
    }


    // TEST CASE - MSP-103:Open Profile
    @When("user click deck {string} on tube {string}")
    public void user_click_deck_deck_on_tube_tube(String deck, String tube) {
        protocolEditorPage.clickTubeOnDeck(deck, tube);
    }
    @When("user click the switch tube button to activate the input box")
    public void user_click_the_switch_tube_button_to_activate_the_input_box() {
        protocolEditorPage.clickSwitchTubeBttn();
    }
    @When("user select {string} on liquid type")
    public void user_select_name_on_liquid_type(String select) throws InterruptedException {
        protocolEditorPage.clickLiquidTypeDropdown();
        protocolEditorPage.selectLiquidType(select);
        Thread.sleep(500);
    }
    @When("user input {int} on starting volume")
    public void user_input_vol_on_starting_volume(int vol) {
        protocolEditorPage.clickStartingVolumeOnDeckPopup();
        protocolEditorPage.setStartingVolume(vol);
    }
    @When("user input {double} on starting volumes")
    public void user_input_volDouble_on_starting_volume(double vol) {
        protocolEditorPage.clickStartingVolumeOnDeckPopup();
        protocolEditorPage.setStartingVolumeDouble(vol);
    }
    @When("user input {double} on starting concentration")
    public void user_input_vol_on_starting_concentration(double vol) {
        protocolEditorPage.clickStartingConcentrationOnDeckPopup();
        protocolEditorPage.setStartingConcentration(vol);
    }
    @When("validate user current position is on new profile page")
    public void validate_user_current_position_is_on_new_profile_page() {
        newProfilePage.verifyNewProfilePage();
    }
    @When("user input {string} as profile name")
    public void user_input_title_as_profile_name(String title) {
        newProfilePage.clickProfileNameBox();
        newProfilePage.setProfileNameTitle(title);
    }
    @When("select {string} as MWCO")
    public void select_option_as_MWCO(String option) {
        newProfilePage.clickMwcoOptionBox();
        newProfilePage.selectMwcoOption(option);
    }
    @When("select {string} as method")
    public void select_option_as_method(String option) {
        newProfilePage.clickMethodOptionBox();
        newProfilePage.selectkMethodOptionBox(option);
    }
    @When("select {string} as buffer source")
    public void select_option_as_buffer_source(String option) {
        newProfilePage.clickBufferSourceBox();
        newProfilePage.setBufferSource(option);
    }
    @When("user click {string} tube on the required labware")
    public void user_click_tube_on_the_required_labware(String tube) {
        newProfilePage.clickTubeInrequiredLabware(tube);
    }
    @When("user input {int} on final volume")
    public void user_inputvol_onfinal_volume(int vol) {
        newProfilePage.clickFinalVolume();
        newProfilePage.setFinalVolume(vol);
    }
    @When("user input {int} on start exchange")
    public void user_input_vol_on_start_exchange(int vol) {
        newProfilePage.clickStartExchangeBox();
        newProfilePage.setStartExchange(vol);
    }
    @When("user input {int} on step size")
    public void user_input_5_on_step_size(int vol) {
        newProfilePage.clickStepSize();
        newProfilePage.setStepSize(vol);
    }
    @When("user input {double} on est final concentration")
    public void user_input_vol_on_est_final_concentration(double vol) {
        newProfilePage.clickFinalConcentration();
        newProfilePage.setFinalConcentration(vol);
    }
    @When("user input {int} on exchange volume")
    public void user_input_25_on_exchange_volume(int vol) {
        newProfilePage.clickExchangeVolume();
        newProfilePage.setExchangeVolume(vol);
    }


    // TEST CASE - MSP-105:Remove Profile
    @When("user doubleclick on protocol title")
    public void user_doubleclick_on_protocol_title() {
        protocolEditorPage.doubleClickProtocolTitle();
    }
    @When("user click option button on the profile card")
    public void user_click_option_button_on_the_profile_card() {
        protocolEditorPage.clickOptionBttnOnProfileCard();
    }


    // TEST CASE - MSP-104:Skip Profile From Queue
    @Then("validate {string} profile card background color changes to {string}")
    public void validate_card_profile_card_background_color_changes_to_color(String card, String color) {
        Assert.assertTrue(protocolEditorPage.verifyCardBackgroundColorGray(card, color));
    }
    @Then("validate {string} profile cards background color changes to {string}")
    public void validate_card_profile_cards_background_color_changes_to_color(String card, String color) {
        Assert.assertTrue(protocolEditorPage.verifyCardBackgroundColorWhite(card, color));
    }


    // TEST CASE - MSP-229:View Selected Tubes each profile
    @When("user click on {string} card")
    public void user_click_on_name_card(String card) {
        protocolEditorPage.clickProfileCard(card);
    }
    @Then("validate tube {string} color is {string}")
    public void validate_tube_name_color_is_color(String tube, String color) {
        Assert.assertTrue(protocolEditorPage.verifyTubeBackgroundColour(tube, color));
    }


    // TEST CASE - MSP-230:Search Labware with Correct Name
    @When("user input {string} on labware search box")
    public void user_input_tube_on_labware_search_box(String tube) {
        protocolEditorPage.inputLabwareSearchBox(tube);
    }
    @When("user clear the labware search box")
    public void user_clear_the_labware_search_box() {
        protocolEditorPage.clearLabwareSearchBox();
    }


    // TEST CASE - MSP-310:Rename Protocol with Unvalid Value
    @Then("validate error popup with message: {string} is appeared")
    public void validate_error_popup_with_message_text_is_appeared(String text) {
        Assert.assertTrue(protocolEditorPage.verifyErrorMessage(text));
    }


    // TEST CASE - MSP-28:Run the Saved Protocol
    @When("user select {string} on chip option")
    public void user_select_option_on_chip_option(String chip) {
        protocolEditorPage.clickChipOption(chip);
    }
    @When("user click Stop button to stop the execution of a protocol")
    public void user_click_Stop_button_to_stop_the_execution_of_a_protocol() throws InterruptedException {
        runtimeStatusPage.clickProtocolStopBttn();
        Thread.sleep(2000);
    }


    // TEST CASE - MSP-312:Run Protocol After Removing Profile Tube Rack
    @When("user click the thrash button on tube rack number {int}")
    public void user_click_the_thrash_button_on_tube_rack_number_rack(int rack) {
        protocolEditorPage.clickTrashOnTubeRack(rack);
    }


    // TEST CASE - MSP-6:Define 50mL tube Rack with Sample Liquid type
    @Then("validate switch tube color button is {string}")
    public void validate_switch_tube_color_button_is_color(String color) {
        Assert.assertTrue(protocolEditorPage.verifySwitchTubeColorBttn(color));
    }
    @Then("validate liquid type box is {string}")
    public void validate_liquid_type_box_is_enableOrDisable(String option) {
        protocolEditorPage.verifyLiquidTypeEnableOrDisable(option);
    }
    @Then("validate starting volume box is {string}")
    public void validate_starting_volume_box_is_enableOrDisable(String option) {
        protocolEditorPage.verifyStartingVolumeEnableOrDisable(option);
    }
    @Then("validate starting concentration box is {string}")
    public void validate_starting_concentration_box_is_enableOrDisable(String option) {
        protocolEditorPage.verifyStartingConcentrationEnableOrDisable(option);
    }
    @Then("validate tube {string} has volume {string} mL")
    public void validate_tube_deckTube_has_volume_vol_mL(String deckTube, String vol) {
        Assert.assertTrue(protocolEditorPage.verifyVolInTube(deckTube, vol));
    }
    @Then("validate tube {string} colour is {string}")
    public void validate_tube_name_colour_is_colour(String tube, String colour) {
        Assert.assertTrue(protocolEditorPage.verifyTubeBackgroundColouur(tube, colour));
    }


    // TEST CASE - MSP-9:Define Chip Rack with Filled Chip
    @Then("validate mwco option {string} is {string}")
    public void validate_mwco_option_chip_is_enabledOrDisabled(String option, String choice) {
        protocolEditorPage.verifyMwcoOptionEnabledOrDisabled(option, choice);
    }
    @Then("validate selected mwco option {string} color is {string}")
    public void validate_selected_mwco_option_select_color_is_color(String mwco, String color) {
        Assert.assertTrue(protocolEditorPage.verifyMwcoSelectedColor(mwco, color));
    }


    // TEST CASE - MSP-89:Validate Hover Labware
    @Then("validate the {string} {string} is zoomed in")
    public void validate_the_rack_is_zoomed_in(String rack, String number) {
        Assert.assertTrue(protocolEditorPage.verifyRackIsZoomedIn(rack, number));
    }

}
    

