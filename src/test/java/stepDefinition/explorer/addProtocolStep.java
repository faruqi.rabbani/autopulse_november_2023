package stepDefinition.explorer;

import org.junit.Assert;

import factory.DriverFactory;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObject.explorer.ExplorerPage;
import pageObject.protocolEditor.ProtocolEditorPage;

public class addProtocolStep {

    private ExplorerPage explorerPage;
    private ProtocolEditorPage protocolEditorPage;

    public addProtocolStep() {
        explorerPage = new ExplorerPage(DriverFactory.getDriver());
        protocolEditorPage = new ProtocolEditorPage(DriverFactory.getDriver());
    }


    // TEST CASE - MSP-211:Validate Explorer Page
    @Then("validate user current position is on explorer page")
    public void validate_user_current_position_is_on_explorer_page() {
        Assert.assertTrue(explorerPage.verifyExplorerPage());
    }
    @Then("validate three dots button is enabled")
    public void validate_three_dots_button_is_enabled() {
        explorerPage.verifyThreeDotsButton();
    }
    @When("user click three dots button")
    public void user_click_three_dots_button() {
        explorerPage.clickThreeDotsBttn();
    }
    @Then("validate explorer search bar is displayed and enabled")
    public void validate_search_bar_is_displayed_and_enabled() {
        explorerPage.verifyExplorerSearchBar();
    }
    @Then("validate sort button is clickable")
    public void validate_sort_bar_is_clickable() {
        explorerPage.verifySortBttn();
    }
    @Then("validate user current position is on protocol editor page")
    public void validate_user_current_position_is_on_protocol_editor_page() {
        Assert.assertTrue(protocolEditorPage.verifyProtocolEditor());
    }

    
    // TEST CASE - MSP-16:Create New Protocol from Protocol Editor
    @When("user input {string} on protocol title")
    public void user_input_text(String text) {
        protocolEditorPage.setProtocolTitle(text);
    }
    @When("user click {string} edit button")
    public void user_click_name_edit_button(String name) {
        protocolEditorPage.clickPulseEditBttn(name);
    }
    @When("user click on {string} dropdown on pulse station pop up box")
    public void user_click_on_tube_dropdown_on_pulse_station_pop_up_box(String tube) {
        protocolEditorPage.clickLeftOrRightTubeAdapter(tube);
    }
    @Then("validate {string} on pulse station 1 have volume {string}")
    public void validate_tube_on_pulse_station_1_have_volume_vol(String tube, String vol) {
        Assert.assertTrue(protocolEditorPage.verifyVolumeOnPulseStation(tube, vol));
    }


    // TEST CASE - MSP-213:Rename Protocol
    @Then("validate three dots dropdown option is displayed")
    public void validate_three_dots_dropdown_option_is_displayed() {
        Assert.assertTrue(explorerPage.verifyThreeDotsOption());
    }
    @When("user input {string} on the rename protocol box")
    public void user_input_text_on_the_rename_protocol_box(String text) {
        explorerPage.clearInputRenameBox();
        explorerPage.setNameOnRenameBox(text);
    }


    // TEST CASE - MSP-313:Rename Protocol with special characters
    @When("user clear the protocol name box")
    public void user_clear_the_protocol_name_box() {
        explorerPage.clearRenameBox();
    }

}
