package pageObject.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObject.BasePOM;

/**
 * POM class that provide various action to interact with the
 * <b>Login Page</b>.
 */
public class LoginPage extends BasePOM {
    By username_txtbox = By.xpath("//input[@placeholder='Username']");
    By password_txtbox = By.xpath("//input[@placeholder='Password']");
    By login_btn = By.xpath("//div[text()='LOGIN']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void typeUsername(String username) {
        act.typeText(username_txtbox, username);
    }

    public void typePassword(String password) {
        act.typeText(password_txtbox, password);
    }

    public void clickLoginBtn() {
        act.clickElement(login_btn);
    }

    public Boolean isLoginButtonDisplayed() {
        return fetch.isElementDisplayed(login_btn);
    }

    public void loginToSystem(String username, String password) {
        typeUsername(username);
        typePassword(password);
        clickLoginBtn();
    }
}
