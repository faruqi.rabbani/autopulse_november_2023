package pageObject.protocolEditor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pageObject.BasePOM;

public class ProtocolEditorPage extends BasePOM {

    By protocolEditorIdentity = By.xpath("//*[@id='fmlxtab-protocoleditor-item-Labware']");
    By inputTitleBox = By.xpath("//*[@id='fmlxtextbox-protocoleditor-protocolname']");
    By switchTubeBttn = By.xpath("//span[@class='MuiButtonBase-root MuiSwitch-switchBase MuiSwitch-colorPrimary PrivateSwitchBase-root MuiSwitch-switchBase MuiSwitch-colorPrimary css-1nr2wod']");
    By liquidTypeDropdown = By.xpath("//div[@id='fmlx-dropdown']/descendant::div[@role='button']");
    By startingVolumeOnDeckPopup = By.xpath("//div[starts-with(text(),'mL')]/preceding-sibling::input");
    By startingConcentration = By.xpath("//div[starts-with(text(),'mg/mL')]/preceding-sibling::input");
    By protocolTitle = By.xpath("//div[@id='commandbarleftcontent-protocoleditor-name']");
    By optionBttnOnProfileCard = By.xpath("//div[@class='card-header  ']/span[@class='card-icon']");
    By labwareSearchBox = By.xpath("//input[@id='fmlxtextbox-protocoleditor-labwaresearch']");
    By tubeSwitchBttn = By.xpath("//span[@class='MuiSwitch-track css-1ju1kxc']");

    public ProtocolEditorPage(WebDriver driver) {
        super(driver);
    }

    public boolean verifyProtocolEditor() {
        return fetch.isElementDisplayed(protocolEditorIdentity);
    }

    public void setProtocolTitle(String name) {
        act.typeText(inputTitleBox, name);
    }

    public void clickPulseEditBttn(String name) {
        By pulseStationEditBttn = By.xpath("//div[text()='" + name + "']/descendant::*[@id='icon-card-pencil1']");
        act.clickElement(pulseStationEditBttn);
    }

    public void clickLeftOrRightTubeAdapter(String tube) {
        By tubeAdapterBttn = By.xpath("//div[text()='" + tube + "']/parent::div/following-sibling::div/div/div/div");
        act.clickElement(tubeAdapterBttn);
    }

    public boolean verifyVolumeOnPulseStation(String tube, String vol) {
        By tubeAdapterBttn = By.xpath("//span[contains(@id, \"" + tube + "\") and starts-with(text(),\"" + vol + "\")]");
        return fetch.isElementDisplayed(tubeAdapterBttn);
    }

    public void clickTubeOnDeck(String deck, String tube) {
        By chosenTubeElement = By.xpath("//*[@id=\"" + deck + '-' + tube + "\"]");
        WebElement chosenTube = driver.findElement(chosenTubeElement);

        action.moveToElement(chosenTube).click(chosenTube).build().perform();
    }

    public void clickSwitchTubeBttn() {
        act.clickElement(switchTubeBttn);
    }

    public void clickLiquidTypeDropdown() {
        act.clickElement(liquidTypeDropdown);
    }

    public void selectLiquidType(String select) {
        By selectedOptionElement = By.xpath("//li[starts-with(text(),\"" + select + "\")]");

        act.moveElement(selectedOptionElement);
        act.clickElement(selectedOptionElement);
    }

    public void clickStartingVolumeOnDeckPopup() {
        act.clickElement(startingVolumeOnDeckPopup);
    }
    
    public void setStartingVolume(int vol) {
        act.typeInt(startingVolumeOnDeckPopup, vol);
    }

    public void setStartingVolumeDouble(double vol) {
        act.typeDouble(startingVolumeOnDeckPopup, vol);
    }

    public void clickStartingConcentrationOnDeckPopup() {
        act.clickElement(startingConcentration);
    }

    public void setStartingConcentration(double vol) {
        act.typeDouble(startingConcentration, vol);
    }

    public void doubleClickProtocolTitle() {
        act.doubleClickElement(protocolTitle);
    }

    public void clickOptionBttnOnProfileCard() {
        act.clickElement(optionBttnOnProfileCard);
    }

    public boolean verifyCardBackgroundColorGray(String card, String color) {
        String colorCardElement = "//div[@id='label-queuecard-profiletitle1']/text()[contains(., \"" + card + "\")]/ancestor::div[@class='card card-disabled  ']";
        WebElement colorCard = driver.findElement(By.xpath(colorCardElement));

        wait.until(ExpectedConditions.visibilityOf(colorCard));

        String expectedColor = color;
        String cardColor = colorCard.getCssValue("background-color");

        return cardColor.equals(expectedColor);
    }

    public boolean verifyCardBackgroundColorWhite(String card, String color) {
        String colorCardElement = "//div[@id='label-queuecard-profiletitle1']/text()[contains(., \"" + card + "\")]/ancestor::div[@class='card   ']";
        WebElement colorCard = driver.findElement(By.xpath(colorCardElement));

        wait.until(ExpectedConditions.visibilityOf(colorCard));

        String expectedColor = color;
        String cardColor = colorCard.getCssValue("background-color");

        return cardColor.equals(expectedColor);
    }

    public void clickProfileCard(String card) {
        By selectedOptionElement = By.xpath("//div[@class='sidepanel-queue-content']/div[contains(.,\"" + card + "\")]");

        act.clickElement(selectedOptionElement);
    }

    public boolean verifyTubeBackgroundColour(String tube, String color) {
        String colorCardElement = "//div[@id=\"" + tube + "\"]";
        WebElement colorCard = driver.findElement(By.xpath(colorCardElement));

        wait.until(ExpectedConditions.visibilityOf(colorCard));
        action.moveToElement(colorCard);

        String expectedColor = color;
        String cardColor = colorCard.getCssValue("background-color");

        return cardColor.equals(expectedColor);
    }

    public void inputLabwareSearchBox(String tube) {
        act.typeText(labwareSearchBox, tube);
    }

    public void clearLabwareSearchBox() {
        WebElement inputBox = driver.findElement(labwareSearchBox);
        action.click(inputBox)
                .click(inputBox)
                .click(inputBox)
                .sendKeys(Keys.BACK_SPACE).build().perform();
    }

    public boolean verifyErrorMessage(String message) {
        String errorPopupElement = "//div[@class='fmlx-toast__content__message'][contains(text(), \"" + message + "\")]";
        WebElement errorPopup = driver.findElement(By.xpath(errorPopupElement));

        wait.until(ExpectedConditions.visibilityOf(errorPopup));

        return errorPopup.isDisplayed();
    }

    public void clickChipOption(String chip) {
        By selectedOptionElement = By.xpath("//label[.=\"" + chip + "\"]");

        act.clickElement(selectedOptionElement);
    }

    public void clickTrashOnTubeRack(int rack) {
        By selectedOptionElement = By.xpath("//div[@class='DeckLayout_container__SLGCt']/div[" + rack + "]//div[@class='delete-labware']");

        act.moveElement(selectedOptionElement);
        act.clickElement(selectedOptionElement);
    }

    public boolean verifySwitchTubeColorBttn(String color) {
        WebElement tubeSwitchElement = driver.findElement(tubeSwitchBttn);

        wait.until(ExpectedConditions.visibilityOf(tubeSwitchElement));
        String expectedColor = color;
        String getSwitchTubeColor = tubeSwitchElement.getCssValue("background-color");

        return getSwitchTubeColor.equals(expectedColor);
    }

    public void verifyLiquidTypeEnableOrDisable(String option) {
        By liquidTypeElement = By.xpath("//input[@class='MuiSelect-nativeInput css-1k3x8v3']");

        switch (option) {
            case "disabled":
                assertFalse("button is enabled", fetch.isElementEnabled(liquidTypeElement));
                break;
            case "enabled":
                assertTrue("button is disabled", fetch.isElementEnabled(liquidTypeElement));
                break;
        }
    }

    public void verifyStartingVolumeEnableOrDisable(String option) {
        switch (option) {
            case "disabled":
                assertFalse("button is enabled", fetch.isElementEnabled(startingVolumeOnDeckPopup));
                break;
            case "enabled":
                assertTrue("button is disabled", fetch.isElementEnabled(startingVolumeOnDeckPopup));
                break;
        }
    }

    public void verifyStartingConcentrationEnableOrDisable(String option) {
        switch (option) {
            case "disabled":
                assertFalse("button is enabled", fetch.isElementEnabled(startingConcentration));
                break;
            case "enabled":
                assertTrue("button is disabled", fetch.isElementEnabled(startingConcentration));
                break;
        }
    }

    public boolean verifyVolInTube(String deckTube, String vol) {
        By liquidTypeElement = By.xpath("//div[@id=\"" + deckTube + "\"]/child::div[starts-with(text(), \"" + vol + "\")]");

        return fetch.isElementDisplayed(liquidTypeElement);
    }

    public boolean verifyTubeBackgroundColouur(String tube, String colour) {
        String colorCardElement = "//div[@id=\"" + tube + "\"]";
        WebElement colorCard = driver.findElement(By.xpath(colorCardElement));

        wait.until(ExpectedConditions.visibilityOf(colorCard));
        action.moveToElement(colorCard);

        String expectedColor = colour;
        String cardColor = colorCard.getCssValue("background-color");
        
        return cardColor.equals(expectedColor);
    }

    public void verifyMwcoOptionEnabledOrDisabled(String chip, String choice) {
        By locatorBttn = By.xpath("//label[.=\"" + chip + "\"]");
        switch (choice) {
            case "disabled":
                assertFalse("button is enabled", fetch.isElementEnabled(locatorBttn));
                break;
            case "enabled":
                assertTrue("button is disabled", fetch.isElementEnabled(locatorBttn));
                break;
        }
    }

    public boolean verifyMwcoSelectedColor(String mwco, String color) {
        String colorMWCOElement = "//div[starts-with(text(), \"" + mwco + "\")]/parent::div/parent::span/preceding-sibling::span";
        WebElement colorMwco = driver.findElement(By.xpath(colorMWCOElement));

        wait.until(ExpectedConditions.visibilityOf(colorMwco));

        String expectedColor = color;
        String cardColor = colorMwco.getCssValue("color");
        
        return cardColor.equals(expectedColor);
    }

    public boolean verifyRackIsZoomedIn(String rack, String number) {
        By rackElement = By.xpath("//div[@id=\"" + rack + "\"]//div[@class='rack zoom-" + number + "']");

        return fetch.isElementDisplayed(rackElement);
    }

}
