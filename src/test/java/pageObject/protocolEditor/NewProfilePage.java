package pageObject.protocolEditor;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObject.BasePOM;

public class NewProfilePage extends BasePOM {

    By newProfileIdentity = By.xpath("//span[@class='protocol-editor-title']");
    By profileNameBox = By.xpath("//input[@id='fmlxtexbox-profileeditor-profilename']");
    By mwcoOptionBox = By.xpath("//div[@id='fmlxdropdown-profileeditor-mwco']/descendant::div[@role='button']");
    By methodOptionBox = By.xpath("//div[@id='fmlxdropdown-profileeditor-method']//div[@role='button']");
    By finalVolumeBox = By.xpath("//div[starts-with(text(),'Final Volume')]/following-sibling::div/descendant::input[@id='fmlx-textinput']");
    By finalConcentrationBox = By.xpath("//div[starts-with(text(),'mg/mL')]/preceding-sibling::input");
    By bufferSourceBox = By.xpath("//div[@id='fmlxdropdown-profileeditor-buffersource']//div[@role='button']");
    By startExchangeBox = By.xpath("//div[starts-with(text(),'Start Exchange')]/following-sibling::div/descendant::input[@id='fmlx-textinput']");
    By stepSizeBox = By.xpath("//div[starts-with(text(),'Step Size')]/following-sibling::div/descendant::input[@id='fmlx-textinput']");
    By exchangeVolume = By.xpath("//div[starts-with(text(),'Exchange Volume')]/following-sibling::div/descendant::input[@id='fmlx-textinput']");

    public NewProfilePage(WebDriver driver) {
        super(driver);
    }

    public boolean verifyNewProfilePage() {
        return fetch.isElementDisplayeds(newProfileIdentity);
    } 

    public void clickProfileNameBox() {
        act.clickElement(profileNameBox);
    }

    public void setProfileNameTitle(String title) {
        act.typeText(profileNameBox, title);
    }

    public void clickMwcoOptionBox() {
        act.clickElement(mwcoOptionBox);
    }

    public void selectMwcoOption(String option) {
        By mwcoOption = By.xpath("//li[starts-with(text(),\"" + option + "\")]");
        
        act.moveElement(mwcoOption);
        act.clickElement(mwcoOption);
    } 

    public void clickMethodOptionBox() {
        act.clickElement(methodOptionBox);
    }

    public void selectkMethodOptionBox(String option) {
        By methodOption = By.xpath("//li[starts-with(text(),\"" + option + "\")]");

        act.moveElement(methodOption);
        act.clickElement(methodOption);
    }

    public void clickTubeInrequiredLabware(String tube) {
        By tubeElement = By.xpath("//div[@class='WellSelection_container__BAOTz']/descendant::div[@id='well-profileeditor-" + tube + "']");
        
        act.moveElement(tubeElement);
        act.clickElement(tubeElement);
    }

    public void clickFinalVolume() {
        act.clickElement(finalVolumeBox);
    }

    public void setFinalVolume(int vol) {
        act.typeInt(finalVolumeBox, vol);
    }

    public void clickFinalConcentration() {
        act.clickElement(finalConcentrationBox);
    }

    public void setFinalConcentration(double vol) {
        act.typeDouble(finalConcentrationBox, vol);
    }

    public void clickBufferSourceBox() {
        act.clickElement(bufferSourceBox);
    }

    public void setBufferSource(String option) {
        By bufferSourceOption = By.xpath("//li[starts-with(text(),\"" + option + "\")]");

        act.moveElement(bufferSourceOption);
        act.clickElement(bufferSourceOption);
    }

    public void clickStartExchangeBox() {
        act.clickElement(startExchangeBox);
    }

    public void setStartExchange(int vol) {
        act.typeInt(startExchangeBox, vol);
    }

    public void clickStepSize() {
        act.clickElement(stepSizeBox);
    }

    public void setStepSize(int vol) {
        act.typeInt(stepSizeBox, vol);
    }

    public void clickExchangeVolume() {
        act.clickElement(exchangeVolume);
    }

    public void setExchangeVolume(int vol) {
        act.typeInt(exchangeVolume, vol);
    }

}
