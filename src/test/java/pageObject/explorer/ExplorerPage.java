package pageObject.explorer;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObject.BasePOM;

public class ExplorerPage extends BasePOM {

    By explorerPageIdentity = By.xpath("//div[@class='MuiGrid-root MuiGrid-item protocol-explorer-command-bar-left css-1wxaqej']");
    By threeDotsBttn = By.xpath("//*[@id='fmlxbutton-protocolexplorer-dots']/button");
    By searchBar = By.xpath("//input[@id='fmlx-textbox']");
    By sortBttn = By.xpath("//*[@id='fmlxbutton-protocolexplorer-sort']/button");
    By threeDotsOption = By.xpath("//div[@class='fmlx-popover']");
    By inputBoxOnRenamePopup = By.xpath("//input[@id='fmlxtexbox-protocolexplorer-rename']");

    public ExplorerPage(WebDriver driver) {
        super(driver);
    }

    public boolean verifyExplorerPage() {
        return fetch.isElementDisplayed(explorerPageIdentity);
    }

    public boolean verifyThreeDotsButton() {
        return fetch.isElementEnabled(threeDotsBttn);
    }

    public void clickThreeDotsBttn() {
        act.clickElement(threeDotsBttn);
    }

    public boolean verifyExplorerSearchBar() {
        return fetch.isElementDisplayed(searchBar) && fetch.isElementEnabled(searchBar);
    }

    public boolean verifySortBttn() {
        return fetch.isElementEnabled(sortBttn);
    }

    public boolean verifyThreeDotsOption() {
        return fetch.isElementDisplayeds(threeDotsOption);
    }

    public void clearInputRenameBox() {
        act.clickElement(inputBoxOnRenamePopup);
        act.typeKeys(inputBoxOnRenamePopup, Keys.BACK_SPACE);
    }

    public void setNameOnRenameBox(String text) {
        act.typeText(inputBoxOnRenamePopup, text);
    }

    public void clearRenameBox() {
        WebElement inputBox = driver.findElement(inputBoxOnRenamePopup);
        action.click(inputBox)
                .click(inputBox)
                .click(inputBox)
                .sendKeys(Keys.BACK_SPACE).build().perform();
    }

}
