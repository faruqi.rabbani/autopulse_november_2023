package pageObject.generalFunction;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Collections of action methods to interact with the UI
 */
public class GeneralAction extends BaseGeneralFunction {

    SupportFunction sprt;

    public GeneralAction(WebDriver driver) {
        super(driver);
        sprt = new SupportFunction(driver);
    }

    /**
     * Click an element
     * 
     * @param locator element locator
     */
    public void clickElement(By locator) {
        sprt.getElementFluently(locator).click();
    }

    /**
     * Click an element with specified timeout
     * 
     * @param locator element locator
     * @param timeout in seconds
     */
    public void clickElement(By locator, int timeout) {
        sprt.getElementFluently(locator, timeout).click();
    }

    /**
     * Simulate user typing on specific element (Usually for input boxes)
     * 
     * @param locator target element locator
     * @param text    text to be typed
     */
    public void typeText(By locator, String text) {
        driver.findElement(locator).sendKeys(text);
    }

    // ukifar

    public void doubleClickElement(By locator) {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        usedElement = sprt.getElementFluently(locator);
        action.doubleClick(usedElement).build().perform();
    }

    public void scrollDownPage(WebElement locator) {
        jse.executeScript("arguments[0].scrollIntoView();", locator);
        wait.until(ExpectedConditions.visibilityOf(locator));
        action.moveToElement(locator);
    }

    public void scrollDownToEndPage() {
        jse.executeScript("window.scrollBy(0, 500);");
    }

    public void dragAndDrop(By dragElementLocator, By dropElementLocator) {
        WebElement dragElement = driver.findElement(dragElementLocator);
        WebElement dropElement = driver.findElement(dropElementLocator);

        wait.until(ExpectedConditions.visibilityOf(dragElement));
        wait.until(ExpectedConditions.visibilityOf(dropElement));

        action.dragAndDrop(dragElement, dropElement).build().perform();
    }

    public void typeKeys(By locator, Keys key) {
        driver.findElement(locator).sendKeys(key);
    }

    public void typeInt(By locator, int number) {
        String numberAsString = String.valueOf(number);
        driver.findElement(locator).sendKeys(numberAsString);
    }

    public void typeDouble(By locator, double number) {
        String numberAsString = String.valueOf(number);
        driver.findElement(locator).sendKeys(numberAsString);
    }

    public void moveElement(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        usedElement = sprt.getElementFluently(locator);
        action.moveToElement(usedElement).build().perform();
    }

}