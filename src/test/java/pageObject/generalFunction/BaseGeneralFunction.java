package pageObject.generalFunction;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Base class for all General Functions
 */
public class BaseGeneralFunction {

    protected WebDriver driver;
    protected Actions action;
    protected JavascriptExecutor jse;
    protected WebDriverWait wait;
    protected WebElement usedElement = null; // Temporary WebElement object to store used element
    protected WebElement dragElement = null;
    protected WebElement dropElement = null;
    protected WebElement scrollElement = null;
    protected List<WebElement> listElement = null;

    public BaseGeneralFunction(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(driver);
        this.jse = (JavascriptExecutor) driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }
}
