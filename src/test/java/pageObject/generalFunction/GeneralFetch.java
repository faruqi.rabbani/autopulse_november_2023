package pageObject.generalFunction;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;

/**
 * Collection of methods to fetch/obtain data from the UI
 */
public class GeneralFetch extends BaseGeneralFunction {

    SupportFunction sprt;
    Wait<WebDriver> localCustomWait; // Global variable to store local fluentWait instance

    public GeneralFetch(WebDriver driver) {
        super(driver);
        sprt = new SupportFunction(driver);
    }

    /**
     * Check whether an element exist or not
     * 
     * @param locator locator
     * @return target element existence status
     */
    public Boolean isElementExist(By locator) {
        List<WebElement> elements = driver.findElements(locator);
        return (elements.size() > 0) ? true : false;
    }

    public boolean isElementDisplayed(By locator) {
        return driver.findElement(locator).isDisplayed();
    }

    public String getText(By locator) {
        localCustomWait = sprt.constructFluentWait();
        usedElement = localCustomWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return usedElement.getText();
    }

    //ukifar

    public boolean isElementEnabled(By locator) {
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator).isEnabled();
    }

    public boolean isElementHighlighted(By highlight, String color) {
        WebElement locatorElement = driver.findElement(highlight);

        wait.until(ExpectedConditions.visibilityOf(locatorElement));
           
        String expectedColor = color;
        String getHighlightedColor = locatorElement.getCssValue("background-color");

        return getHighlightedColor.equals(expectedColor);
    }

    public boolean verifyTextColor(By highlight, String color) {
        WebElement locatorElement = driver.findElement(highlight);

        wait.until(ExpectedConditions.visibilityOf(locatorElement));
           
        String expectedColor = color;
        String getHighlightedColor = locatorElement.getCssValue("color");

        return getHighlightedColor.equals(expectedColor);
    }

    public Boolean isElementNotExist(By locator) {
        sprt.setImplicitWaitMillis(50);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        sprt.resetImplicitWait();
        List<WebElement> elements = driver.findElements(locator);
        return (elements.size() > 0) ? true : false;
    }

    public Boolean isElementNotExistNoWait(By locator) {
        sprt.setImplicitWaitMillis(50);
        List<WebElement> elements = driver.findElements(locator);
        sprt.resetImplicitWait();
        return (elements.size() > 0) ? true : false;
    }

    public boolean isElementDisplayeds(By locator) {
        WebElement element = driver.findElement(locator);
        sprt.setExplicitWaitSecVisibility(element);
        return element.isDisplayed();
    }

}
