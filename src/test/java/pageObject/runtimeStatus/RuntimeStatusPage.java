package pageObject.runtimeStatus;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObject.BasePOM;

public class RuntimeStatusPage extends BasePOM {

    By protocolStopBttn = By.xpath("//*[@id='fmlxbutton-runtime-stop']");

    public RuntimeStatusPage(WebDriver driver) {
        super(driver);
    }

    public void clickProtocolStopBttn() {
        act.clickElement(protocolStopBttn);
    }

}
