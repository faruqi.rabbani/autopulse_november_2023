package pageObject;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObject.generalFunction.GeneralAction;
import pageObject.generalFunction.GeneralFetch;
import pageObject.generalFunction.SupportFunction;

/**
 * Base class for all POM classes/files.
 */
public class BasePOM {

    protected WebDriver driver; // Main selenium driver
    protected Actions action; // Grants low level Selenium's Actions class function.
    protected GeneralAction act; // Grants general pre-made Actions.
    protected GeneralFetch fetch; // Grants general pre-made data fetching process.
    protected SupportFunction sprt; // Grants general supporting functions.
    protected WebElement usedElement = null; // Temporary WebElement object to store used element
    protected List<WebElement> listElement = null;
    protected WebDriverWait wait;

    public BasePOM(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(this.driver);
        this.act = new GeneralAction(this.driver);
        this.fetch = new GeneralFetch(this.driver);
        this.sprt = new SupportFunction(this.driver);
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }
}
